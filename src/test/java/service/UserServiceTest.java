package service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.service.implementation.UserServiceImpl;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    //    @Before
//    public void setUp() throws Exception {
//        userService = new UserServiceImpl(userRepository);
//    }
    @InjectMocks
    UserService userService = new UserServiceImpl();
    //    @Mock // ???
//    private UploadAvatarUtil uploadAvUtil;
//    private UserService userService;
    @Mock
    private UserRepository userRepository;

//    @Before
//    public void doSetup() {
//        userRepository = mock(UserRepository.class);
//        userService = new UserServiceImpl(userRepository);
//    }

    // Custom User for tests;
    private static User createUser() {
        User user = new User();
        user.setLogin("lolo");
        user.setFirstname("lolo");
        user.setLastname("lolo");
        user.setEmail("lolo");
        return user;
    }

    private static UserDTO createUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setLogin("lolo");
        userDTO.setFirstname("lolo");
        userDTO.setLastname("lolo");
        userDTO.setEmail("lolo");
        return userDTO;
    }

    /* Testing of the method below
    * public User findByLogin(String login)
    * */

    @Test
    public void findByLogin() {
        Mockito.doReturn(createUser()).when(userRepository).findOne(Mockito.anyString());
        // when
        final User user = userService.findByLogin(Mockito.anyString());
        // then
//        assertThat(user.getLogin()).isEqualTo(login);
        assertThat(user).isNotNull();
//        return user;
    }

    /* Testing of the method below
    *  public User addUser(UserDTO userDTO)
    * */

   /* @Test
    public void addUser() {
        // given
        final UserDTO userDTO = createUserDTO();
        // when
        User user = userService.addUser(userDTO);
        // then
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
//        return user;
    }*/



    /* Testing of the method below
    *  public String uploadAvatar(MultipartFile file, String login); // UploadAvatarUtil
    * */

//    public String uploadAvatar() {
//        MultipartFile file = new MockMultipartFile(Mockito.anyString(), Mockito.any());
//        String login;
//        return UploadAvatarUtil.uploadAvatar(file, login, environment.getProperty("image.upload.path"));
//    }

}
