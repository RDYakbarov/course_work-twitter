/**
 * Created by RDYakbarov on 07.05.15.
 */
var $tweetText;
var tweetTextHtmlBeforeEdit;
var $editingTweet;

//handlers
{
    function removeTweet(e) {
        if (e) {
            e.preventDefault()
        }
        var tweetId = $(this).closest('.entry').attr('id');
        $.post('/ajax/tweet/delete', {
            tweetId: tweetId
        }).success(function () {
            console.log('successfully deleted');
            $('.entry[id=' + tweetId + ']').hide(300, function () {
                this.remove();
            });
        }).error(function (data) {
            console.error(data);
        })
    }

    function editTweet(e) {
        if (e) {
            e.preventDefault()
        }
        var $tweetText;
        tweetTextHtmlBeforeEdit =
            ($tweetText = ($editingTweet = $(this).closest('.entry'))
                .find('.tweet-text p')).attr('contenteditable', "true").html();
        $tweetText.html($tweetText.text())
    }

    function tweetTextValueChanged(e) {
        var $self = $(this);
        if ($self.text().length >= 140) {
            e.preventDefault();
        }
        if (e.which == 13) {
            e.preventDefault();
            var mentionsIcon = '<i class="icon-user"></i>';
            var tweetText = $editingTweet.find('.tweet-text p').text().trim();
            if (tweetText) {
                console.log(tweetText);
                $.ajax({
                    url: '/ajax/tweet/edit',
                    type: 'POST',
                    data: {
                        tweetId: $editingTweet.attr('id'),
                        tweetText: tweetText
                    },
                    async: false,
                    success: function (data) {
                        console.log('success');
                        console.log(data);
                        $self.focusout();
                        $self.html(data.text);
                        $editingTweet.find('.mentions').html(mentionsIcon + data.mentionedCount);
                        tweetTextHtmlBeforeEdit = null;
                        $editingTweet = null;
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
                //$editingTweet = null;
                //tweetTextHtmlBeforeEdit = null;
            } else {
                console.log('empty tweet text');
            }
        }
    }

    function focusOutAction() {
        $(this).attr('contenteditable', "false");
        if (tweetTextHtmlBeforeEdit) {
            $(this).html(tweetTextHtmlBeforeEdit)
        }
    }

    function pasteAction(e) {
        var $self = $(this);
        var textToPaste = e.originalEvent.clipboardData.getData('Text');
        if (($self.text().trim().length + textToPaste.length) > 140) {
            if (e) e.preventDefault();
            else return false;
        }
    }
}

//it is like $(document).ready(...)
$(document).ready(function () {

    var $tweetModal = $('#addTweet');
    var $sendTweetBtn = $('#js-sendTweet');
    if (window.location.pathname == '/') {
        $('.js-toplinks li:first').before('<li><a href="#" class="js-createTweet"><div>Твитнуть</div></a></li>');
    }

    // Char count function for textarea (preValidation)
    var characterCount = function (textArea, fieldCount) {
        var myField = document.getElementById(textArea);
        var myLabel = document.getElementById(fieldCount);
        //if they are not found
        if (!myField || !myLabel) {
            return false
        }
        // catches errors
        var maxCountOfChars = myField.maxLengh;
        if (!maxCountOfChars) {
            maxCountOfChars = myField.getAttribute('maxlength');
        }
        if (!maxCountOfChars) {
            return false
        }
        var curCountOfChars = myField.value.length;
        var remainingChars = maxCountOfChars - curCountOfChars;
        myLabel.innerHTML = remainingChars + " / " + maxCountOfChars;
        return curCountOfChars;
    };
    characterCount('tweet', 'charCountLabel');

    //initialize handlers
    {
        $('.js-edit-tweet').click(editTweet);
        $('.js-remove-tweet').click(removeTweet);
        $tweetText = $('.tweet-text p');
        $tweetText.on('input propertychange keypress', tweetTextValueChanged);
        $tweetText.on('paste', pasteAction);
        $tweetText.focusout(focusOutAction);

        $('.js-createTweet').click(function () {
            $tweetModal.modal('show');
            return false;
        });

        //TODO make adding logins of users, which have access to tweet (with autocomplete)
        if ($sendTweetBtn) {
            $sendTweetBtn.click(function (event) {
                event.preventDefault();
                var tweetText = $(".js-text").val();
                if (!(tweetText.trim() == '')) {
                    $.ajax({
                        type: "POST",
                        url: "/ajax/tweet/add",
                        data: {
                            text: tweetText
                        },
                        success: function (data) {
                            var $data = $(data);
                            $('#posts').prepend($data);
                            $('.tweet-text:first p')
                                .on('input propertychange keypress', tweetTextValueChanged)
                                .on('paste', pasteAction)
                                .focusout(focusOutAction)
                                .end().find('.js-edit-tweet').click(editTweet)
                                .end().find('.js-remove-tweet').click(removeTweet);
                            $('#tweet').val("");
                            $sendTweetBtn.prop('disabled', 'disabled');
                            $tweetModal.modal('hide');
                        },
                        error: function (data) {
                            console.error(data);
                        }
                    });
                } else {
                    $sendTweetBtn.prop('disabled', 'disabled');
                }
            });
            $('textarea[id="tweet"]').on('input propertychange', function () {
                $sendTweetBtn.prop('disabled', !(characterCount('tweet', 'charCountLabel') > 0));
            });
        }
    }
});