/**
 * Created by RDYakbarov on 09.05.15.
 */
function addToFavourites(e) {
    if (e) {
        e.preventDefault()
    }
    var icon = '<i class="icon-star"></i>';
    var $self = $(this);
    var $post = $self.closest('.entry');
    $.post('/ajax/favourites/add', {
        tweetId: $post.attr('id')
    }).success(function () {
        $self.html(icon + ($post.hasClass('tweet') ? '' : ' ') + (parseInt($self.text()) + 1));
        $self.children().unwrap();
        console.log('added to favourites');
    }).error(function (data) {
        console.error('not added to favourites');
        console.error(data);
    })
}
$(document).ready(function () {
    $('.js-favourite-add').click(addToFavourites)
});