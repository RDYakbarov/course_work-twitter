/**
 * Created by RDYakbarov on 06.05.15.
 */
function addRetweet(e) {
    if (e) {
        e.preventDefault();
    }
    var icon = '<i class="icon-retweet"></i>';
    var $self = $(this);
    var $post = $self.closest('.entry');
    $.ajax({
        type: 'POST',
        url: '/ajax/retweet/add',
        contentType: "application/json",
        data: JSON.stringify({
            tweetId: $post.attr('id')
        }),
        success: function (data) {
            $self.html(icon + (parseInt($self.text()) + 1));
            $self.children().unwrap();
            $('#posts').prepend($(data));
            var $retweet = $('.retweet:first');
            $retweet
                .find('.js-retweet-remove').click(removeRetweet)
                .end().find('.js-favourite-add').click(addToFavourites);
            console.log('success');
        },
        error: function (data) {
            console.error('error');
            console.error(data);
        }
    });
}
function removeRetweet(e) {
    if (e) {
        e.preventDefault()
    }
    var $post = $(this).closest('.entry');
    var jqxhr = $.post('/ajax/retweet/delete', {
        tweetId: $post.attr('id')
    });
    jqxhr.success(function () {
        console.log('successfully deleted');
        $post.hide(300, function () {
            $post.remove();
        })
    }).error(function (data) {
        console.error(data);
    })
}
$(document).ready(function () {
    $('.js-retweet-add').click(addRetweet);
    $('.js-retweet-remove').click(removeRetweet)
});