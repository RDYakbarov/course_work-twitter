/**
 * Created by RDYakbarov on 13.04.15.
 */
$(document).ready(function () {

    if (window.location.pathname == '/') {
        $(window).scroll(appendTweetsOnScroll);
    }

    var getMoreTweetsAjaxInProgress = false;

    //handlers
    {
        function getMoreTweets() {
            $.ajax({
                type: 'POST',
                url: '/ajax/tweet/more',
                data: {
                    //page: pageCount++,
                    //size: 20,
                    //sort: 'time,desc'
                    tweetNum: $('.tweet').length,
                    retweetNum: $('.retweet').length
                },
                beforeSend: function () {
                    getMoreTweetsAjaxInProgress = true;
                },
                success: function (data) {
                    getMoreTweetsAjaxInProgress = false;
                    if (data.length > 0) {
                        $('#posts').append(data);
                        $('.js-edit-tweet,.js-remove-tweet,.js-retweet-add,.js-retweet-remove,.js-favourite-add')
                            .unbind('click');
                        /*.each(function (i, elem) {
                         var $self = $(this);
                         console.log(this);
                         if ($self.hasClass('.js-edit-tweet')) $self.click(editTweet);
                         if ($self.hasClass('.js-remove-tweet')) $self.click(removeTweet);
                         if ($self.hasClass('.js-retweet-add')) $self.click(addRetweet);
                         if ($self.hasClass('.js-retweet-remove')) $self.click(removeRetweet)
                         });*/
                        $('.js-edit-tweet')
                            .click(editTweet);
                        $('.js-remove-tweet')
                            .click(removeTweet);
                        $('.js-retweet-add')
                            .click(addRetweet);
                        $('.js-retweet-remove')
                            .click(removeRetweet);
                        $('.js-favourite-add')
                            .click(addToFavourites);
                        $('.tweet-text p')
                            .unbind('keypress focusout')
                            .keypress(tweetTextValueChanged)
                            .focusout(focusOutAction);
                        getMoreTweetsAjaxInProgress = false;
                    } else {
                        $(window).unbind('scroll', appendTweetsOnScroll)
                    }
                },
                error: function (data) {
                    console.error('tweets loading error');
                    console.error(data);
                    getMoreTweetsAjaxInProgress = false;
                }
            })
        }

        function appendTweetsOnScroll() {
            if ($(window).scrollTop() + $(window).height() >= $(document).height() - 1000 && !getMoreTweetsAjaxInProgress) {
                getMoreTweets();
            }
        }
    }

});