<#assign security=JspTaglibs['http://www.springframework.org/security/tags']>
<@security.authorize access='isAnonymous()'>
    <@security.authentication property='principal' var='principal' scope='page'/>
</@security.authorize>
<@security.authorize access="isAuthenticated()">
    <@security.authentication property='principal.login' var='principal' scope='page'/>
</@security.authorize>
<#assign creator=answer.creator.login>
<li class="comment" id="${answer.tweetId}">
    <div class="comment-wrap clearfix">
        <div class="comment-meta">
            <div class="comment-author vcard">
                <span class="comment-avatar clearfix">
                    <a href="/${creator}">
                        <img title="${creator}"
                             src="${answer.creator.avatarURL!defPic}"
                             class="avatar avatar-60 photo" height="60" width="60">
                    </a>
                </span>
            </div>
        </div>
        <div class="comment-content clearfix">
            <div class="comment-author">
                <a href="/${creator}"
                   rel="external nofollow" class="url">${creator}</a>
                <span>
                    <i><#--${answer.time?number_to_datetime?string}-->${answer.timeView}</i>
                </span>
            </div>
            <p>${answer.text}</p>
        <#if creator == principal>
            <a class="comment-reply-link js-answer-remove" href="#"><i class="icon-remove"></i></a>
        </#if>
        </div>
    </div>
</li>