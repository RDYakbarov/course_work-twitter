<#include "../template.ftl">
<#macro body>
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_half nobottommargin">
                <div class="error404 center">404</div>
            </div>

            <div class="col_half nobottommargin col_last">

                <div class="heading-block nobottomborder">
                    <h4>Пользователя ${login} не существует</h4>
                <#--<span>Try searching for the best match or browse the links below:</span>-->
                </div>

            </div>

        </div>

    </div>

</section>
</#macro>
<@main title="Пользователь не найден"/>