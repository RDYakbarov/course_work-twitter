<#include '../template.ftl'>
<#macro body>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div id="posts" class="post-grid grid-2 clearfix">
                <#include "../postsInclude.ftl">
            </div>
        </div>
    </div>
</section>
</#macro>
<@main title='Избранное'/>