<#assign security=JspTaglibs['http://www.springframework.org/security/tags']>
<@security.authorize access='isAnonymous()'>
    <@security.authentication property='principal' var='principal' scope='page'/>
</@security.authorize>
<@security.authorize access="isAuthenticated()">
    <@security.authentication property='principal.login' var='principal' scope='page'/>
    <@security.authentication property='principal' var='principalObj' scope='page'/>
</@security.authorize>
<#function alreadyDidRetweet retweets>
    <#list retweets as retweet>
        <#if retweet.creator == principal>
            <#return true>
        </#if>
    </#list>
    <#return false>
</#function>
<#if tweets?size!=0>
    <#list tweets as tweet>
        <#assign class = tweet.class.simpleName?lower_case>
        <#assign id = tweet.tweetId>
    <div class="entry clearfix ${class}" id="${id}">
        <#if tweet.text??>
            <#assign creator = tweet.creator.login>
            <div class="entry-title">
                <h2>
                    <a href="/${creator}">${creator}</a>
                </h2>
            </div>
            <ul class="entry-meta clearfix">
                <li>
                    <i class="icon-calendar3"></i>${tweet.timeView}
                <#--${(tweet.time?number_to_datetime?string)!"неизвестно"}-->
                </li>
                <li class="mentions"><i class="icon-user"></i>${(tweet.mentionedUsers?size)!0}</li>
                <li>
                    <#if principal='anonymousUser' || creator == principal || alreadyDidRetweet(tweet.retweets)>
                        <i class="icon-retweet"></i>${(tweet.retweets?size)!0}
                    <#else>
                        <a href="#" class="js-retweet-add"><i class="icon-retweet"></i>${(tweet.retweets?size)!0}</a>
                    </#if>
                </li>
                <li>
                    <#if principal='anonymousUser' || creator == principal || tweet.whoAddedToFavourites?seq_contains(principalObj)>
                        <i class="icon-star"></i>${(tweet.whoAddedToFavourites?size)!0}
                    <#else>
                        <a href="#" class="js-favourite-add"><i
                                class="icon-star"></i>${(tweet.whoAddedToFavourites?size)!0}</a>
                    </#if>
                </li>
                <li>
                    <a href="/tweet/${id}"><i class="icon-comments"></i>${(tweet.answers?size)!0}</a>
                </li>
                <#if creator == principal>
                    <li><a href="#"><i class="icon-edit js-edit-tweet"></i></a></li>
                    <li><a href="#"><i class="icon-remove js-remove-tweet"></i></a></li>
                </#if>
            </ul>
            <div class="entry-content tweet-text">
                <div class="author-image">
                    <a href="/${creator}">
                        <img class="img-rounded" src="${tweet.creator.avatarURL!defPic}" alt="${creator}">
                    </a>
                </div>
                <p>${tweet.text}</p>
            </div>
        <#--<a class="more-link" href="/tweet/${id}">Подробнее</a>-->
        <#else>
            <#assign original = tweet.tweet>
            <#assign retweeter = tweet.creator>
            <#assign creator = original.creator.login>
            <div class="entry-title">
                <h2><a href="/${retweeter}">${retweeter}</a><label>&nbspретвитнул</label></h2>
            </div>
            <ul class="entry-meta">
                <li>
                    <i class="icon-calendar3"></i><#--${(tweet.time?number_to_datetime?string)!"неизвестно"}-->${tweet.timeView}
                </li>
                <#if retweeter == principal>
                    <li><a href="#"><i class="icon-remove js-retweet-remove"></i></a></li>
                </#if>
            </ul>
            <div class="entry-c">
                <blockquote>
                    <p>${original.text}</p>
                    <footer>
                        <a href="/${creator}">${creator}</a>
                        / <i class="icon-calendar3"></i> ${original.timeView}
                        / <i class="icon-user"></i> ${(original.mentionedUsers?size)!0}
                        <#if principal='anonymousUser' ||  principal == retweeter || alreadyDidRetweet(original.retweets)>
                            / <i class="icon-retweet"></i> ${(original.retweets?size)!0}
                        <#else>
                            / <a href="#" class="js-retweet-add"><i
                                class="icon-retweet"></i> ${(original.retweets?size)!0}</a>
                        </#if>
                        <#if principal='anonymousUser' || creator == principal || original.whoAddedToFavourites?seq_contains(principalObj)>
                            / <i class="icon-star"></i> ${(original.whoAddedToFavourites?size)!0}
                        <#else>
                            / <a href="#" class="js-favourite-add"><i
                                class="icon-star"></i> ${(original.whoAddedToFavourites?size)!0}</a>
                        </#if>
                        / <a href="/tweet/${original.tweetId}"><i
                            class="icon-comments"></i> ${(original.answers?size)!0}
                    </a>
                    </footer>
                </blockquote>
            </div>
        </#if>
    </div>
    </#list>
<#elseif user??>
<div class="heading-block center">
    <#if user.login == principal>
        <h1>У вас пока нет твитов</h1>
        <span>Неужели вам нечем поделиться?</span>
    <#else>
        <h1>Пользователь скрыл свои твиты</h1>
        <span>При добавлении твита есть возможность указать людей, которые смогут видеть данный твит</span>
    </#if>
</div>
</#if>
