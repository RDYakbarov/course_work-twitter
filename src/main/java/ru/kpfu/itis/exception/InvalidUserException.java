package ru.kpfu.itis.exception;

/**
 * Created by RDYakbarov on 09.05.15.
 */
public class InvalidUserException extends RuntimeException {

    public InvalidUserException(String s) {
        super(s);
    }
}
