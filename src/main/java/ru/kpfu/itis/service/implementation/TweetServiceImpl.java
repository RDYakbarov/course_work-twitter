package ru.kpfu.itis.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.dto.TweetDTO;
import ru.kpfu.itis.model.*;
import ru.kpfu.itis.repository.RetweetRepository;
import ru.kpfu.itis.repository.TweetRepository;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.TweetService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.text.MessageFormat.format;
import static java.util.Collections.sort;
import static ru.kpfu.itis.util.AuthenticationUtil.getAuthUser;
import static ru.kpfu.itis.util.AuthenticationUtil.isAuthenticated;
import static ru.kpfu.itis.util.Constants.PAGE_SIZE;

/**
 * Created by RDYakbarov on 06.04.2015.
 */
@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class TweetServiceImpl implements TweetService {

    @Autowired
    ExecutorService executorService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TweetRepository tweetRepository;

    @Autowired
    RetweetRepository retweetRepository;

    public List<AbstractTweet> findByCreator(final User creator, final Sort sort) throws InterruptedException, ExecutionException {
        List<AbstractTweet> tweetList = new ArrayList<>();
        final String creatorLogin = creator.getLogin();
        final boolean authenticated = isAuthenticated();
        final String userLogin = authenticated ? getAuthUser().getLogin() : null;
        final boolean userIsCreator = creatorLogin.equals(userLogin);
        Future<List<Tweet>> tweetsFuture = executorService.submit(new Callable<List<Tweet>>() {
            public List<Tweet> call() throws Exception {
                List<Tweet> tweets;
                if (authenticated) {
                    tweets = userIsCreator ?
                            tweetRepository.findByCreatorAndMainTweetIsNull(creator, sort)
                            : tweetRepository.findByCreatorAccessedTo(creatorLogin, userLogin, sort);
                } else tweets = tweetRepository.findByCreatorAndMainTweetIsNullAndAccessibleToIsNull(creator, sort);
                return tweets;
            }
        });
        Future<List<Retweet>> retweetsFuture = executorService.submit(new Callable<List<Retweet>>() {
            public List<Retweet> call() throws Exception {
                List<Retweet> retweets;
                if (authenticated) {
                    retweets = userIsCreator ?
                            retweetRepository.findByCreatorAndTweet_MainTweetIsNull(creatorLogin, sort)
                            : retweetRepository.findByRetweeterAccessedTo(creatorLogin, userLogin, sort);
                } else retweets = retweetRepository.findPubRetweets(creatorLogin, sort);
                return retweets;
            }
        });
        tweetList.addAll(tweetsFuture.get());
        tweetList.addAll(retweetsFuture.get());
        sort(tweetList);
        return tweetList;
    }

    @Override
    //to override readOnly property
    @Transactional
    public Tweet save(Tweet tweet) {
        return tweetRepository.save(tweet);
    }

    @Override
    @Transactional
    public Tweet save(TweetDTO tweetDTO) {
        Tweet tweet = new Tweet();
        tweet.setCreator(getAuthUser());
        String text = tweetDTO.getText();
        tweet.setCoordinates(tweetDTO.getCoordinates());
        Long mainTweetId = tweetDTO.getMainTweetId();
        if (mainTweetId != null) {
            tweet.setMainTweet(tweetRepository.findOne(mainTweetId));
        }
        List<String> mentionedUsersLogins = parseMentionedUsers(text);
        Set<User> mentionedUsers = null;
        if (mentionedUsersLogins.size() != 0) {
            mentionedUsers = userRepository.findByLoginIn(mentionedUsersLogins);
            tweet.setMentionedUsers(mentionedUsers);
            String login;
            for (User mentionedUser : mentionedUsers) {
                login = mentionedUser.getLogin();
                text = text.replaceAll("@" + login, format("<a href=\"/{0}\">@{0}</a>", login));
            }
        }
        tweet.setText(text);
        List<String> accessibleTo = tweetDTO.getAccessibleTo();
        if (accessibleTo != null) {
            tweet.setAccessibleTo(userRepository.findByLoginIn(accessibleTo));
        }
        tweet = tweetRepository.save(tweet);
        Long tweetId = tweet.getTweetId();
        if (mentionedUsers != null) {
            for (User mentionedUser : mentionedUsers) {
                tweetRepository.addMention(tweetId, mentionedUser.getLogin());
            }
        }
        return tweet;
    }

    private List<String> parseMentionedUsers(String text) {
        List<String> mentionedUsersLogins = new ArrayList<>();
        Matcher matcher = Pattern.compile("([\\W_&&[^@]]+|^)@([A-Za-z0-9]+)").matcher(text);
        while (matcher.find()) {
            mentionedUsersLogins.add(matcher.group(2));
        }
        return mentionedUsersLogins;
    }

    @Override
    @Transactional
    public Retweet save(Retweet retweet) {
        retweet.setCreator(getAuthUser().getLogin());
        retweet = retweetRepository.save(retweet);
        if (retweet.getTweet() == null) {
            Tweet tweet = tweetRepository.findOne(retweet.getTweetId());
            tweet.getRetweets().add(retweet);
            retweet.setTweet(tweet);
        }
        if (retweet.getRetweeter() == null) {
            retweet.setRetweeter(getAuthUser());
        }
        return retweet;
    }

    @Override
    @Transactional
    public void addToFavourites(long tweetId) {
        tweetRepository.addToFavourites(tweetId, getAuthUser().getLogin());
    }

    @Override
    @Transactional
    public void deleteTweet(Long tweetId) {
        tweetRepository.delete(tweetId);
    }

    @Override
    @Transactional
    public void deleteRetweet(Long tweetId) {
        retweetRepository.delete(new RetweetId(tweetId, getAuthUser().getLogin()));
    }

    @Override
    @Transactional
    public void update(Long id, String text) {
        tweetRepository.updateTweetText(id, text);
    }

    @Override
    @Transactional
    public Tweet update(Tweet tweet, String newText) {
        List<String> mentionedUsersLogins = parseMentionedUsers(newText);
        Long tweetId = tweet.getTweetId();
        tweetRepository.deleteMentions(tweetId);
        Set<User> mentionedUsers = null;
        if (mentionedUsersLogins.size() != 0) {
            mentionedUsers = userRepository.findByLoginIn(mentionedUsersLogins);
            String login;
            for (User mentionedUser : mentionedUsers) {
                login = mentionedUser.getLogin();
                newText = newText.replaceAll("@" + login, format("<a href=\"/{0}\">@{0}</a>", login));
                tweetRepository.addMention(tweetId, login);
            }
        }
        tweet.setText(newText);
        tweet.setMentionedUsers(mentionedUsers);
        return tweetRepository.save(tweet);
    }

    @Override
    public List<Tweet> getFavourites() {
        return tweetRepository.findFavouritesByUser(getAuthUser().getLogin());
    }

    @Override
    public Page<Tweet> getTweetsPage(Pageable pageable) {
        return tweetRepository.findByMainTweetIsNull(pageable);
    }

    @Override
    public Tweet findById(Long id) {
        return tweetRepository.findOne(id);
    }

    @Override
    public Page<Tweet> findTweetsAccessedTo(User user, Pageable pageable) {
        return tweetRepository.findTweetsAccessedTo(user, pageable);
    }

    @Override
    public List<AbstractTweet> tweetsAndRetweetsAccessedTo(final User user, final int tweetNum, final int retweetNum) {
        List<Tweet> tweets = null;
        List<Retweet> retweets = null;
        Future<List<Tweet>> tweetsFuture = executorService.submit(new Callable<List<Tweet>>() {
            public List<Tweet> call() throws Exception {
                return tweetRepository.findTweetsAccessedTo(user, tweetNum, PAGE_SIZE);
            }
        });
        Future<List<Retweet>> retweetsFuture = executorService.submit(new Callable<List<Retweet>>() {
            public List<Retweet> call() throws Exception {
                return retweetRepository.findRetweetsAccessedTo(user, retweetNum, PAGE_SIZE);
            }
        });
        try {
            tweets = tweetsFuture.get();
            retweets = retweetsFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return genTweetsPage(tweets, retweets);
    }


    private List<AbstractTweet> genTweetsPage(List<Tweet> tweets, List<Retweet> retweets) {
        if (tweets == null || retweets == null) {
            return null;
        }
        List<AbstractTweet> tweetList = new ArrayList<>(40);
        tweetList.addAll(tweets);
        tweetList.addAll(retweets);
        sort(tweetList);
        int listLength = tweetList.size();
        tweetList = tweetList.subList(0, 20 > listLength ? listLength : 20);
        return tweetList;
    }
}
