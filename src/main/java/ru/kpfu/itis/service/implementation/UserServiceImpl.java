package ru.kpfu.itis.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.repository.UserRepository;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.FormMapper;
import ru.kpfu.itis.util.PassEncryptionUtil;
import ru.kpfu.itis.util.UploadAvatarUtil;

/**
 * Created by RDYakbarov on 16.03.2015.
 */
@Service
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
@PropertySource(value = "classpath:common.properties", ignoreResourceNotFound = true)
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    Environment environment;

//    /*
//    * For Tests
//    */
//    public UserServiceImpl(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }

    @Override
    public boolean userExists(String login) {
        return userRepository.findByLoginIgnoreCase(login) != null;
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findOne(login);
    }

    @Override
    public User findByLoginIgnoreCase(String login) {
        return userRepository.findByLoginIgnoreCase(login);
    }

    @Override
    @Transactional
    public User addUser(UserDTO userDTO) {
        String salt = PassEncryptionUtil.generateSalt();
        User user = FormMapper.userFormToUser(userDTO);
        user.setSalt(salt);
        user.setPassword(PassEncryptionUtil.encrypt(userDTO.getPassword(), salt));
        return userRepository.save(user);
    }

    @Override
    public String uploadAvatar(MultipartFile file, String login) {
        return UploadAvatarUtil.uploadAvatar(file, login, environment.getProperty("image.upload.path"));
    }

    @Override
    @Transactional
    public User save(User user) {
        return userRepository.save(user);
    }

}
