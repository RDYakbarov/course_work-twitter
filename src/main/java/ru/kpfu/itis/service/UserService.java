package ru.kpfu.itis.service;

import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.model.User;

/**
 * Created by RDYakbarov on 16.03.2015.
 */
public interface UserService  {

    boolean userExists(String login);

    User findByLogin(String login);

    User findByLoginIgnoreCase(String login);

    User addUser(UserDTO userDTO);

    String uploadAvatar(MultipartFile file, String login);

    User save(User user);


}
