package ru.kpfu.itis.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.dto.UserDTO;

/**
 * Created by RDYakbarov on 05.04.2015.
 */
public class UserRegistrationValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return UserDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTO userDTO = (UserDTO) target;
        String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9])*(\\.[A-Za-z]{2,})";
        String loginPattern = "[A-Za-z0-9]+";
        String passwordPattern = "[A-Za-z0-9]+";
        if (userDTO.getLogin().trim().length() == 0)
            errors.rejectValue("login", "login.empty");
        else if (!userDTO.getLogin().trim().matches(loginPattern))
            errors.rejectValue("login", "login.invalid");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstname", "firstname.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastname", "lastname.empty");
        String email = userDTO.getEmail().trim();
        if (email.equals(""))
            errors.rejectValue("email", "email.empty");
        else if (!email.matches(emailPattern))
            errors.rejectValue("email", "email.invalid");
        String password = userDTO.getPassword().trim();
        String repassword = userDTO.getRepassword().trim();
        if (password.equals(""))
            errors.rejectValue("password", "password.empty");
        else if (!password.matches(passwordPattern))
            errors.rejectValue("password", "password.invalid");
        else if (repassword.equals(""))
            errors.rejectValue("repassword", "repassword.empty");
        else if (!userDTO.getPassword().equals(userDTO.getRepassword()))
            errors.rejectValue("password", "password.notsame");
    }
}
