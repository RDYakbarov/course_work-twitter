package ru.kpfu.itis.repository;

import ru.kpfu.itis.model.Tweet;
import ru.kpfu.itis.model.User;

import java.util.List;

/**
 * Created by RDYakbarov on 02.05.15.
 */
public interface TweetRepositoryCustom {

    List<Tweet> findTweetsAccessedTo(User user, int offset, int maxResult);

}
