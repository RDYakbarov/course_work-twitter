package ru.kpfu.itis.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import javax.persistence.*;

/**
 * Created by RDYakbarov on 15.04.15.
 */
@Entity
@Table(name = "retweets")
@IdClass(RetweetId.class)
public class Retweet extends AbstractTweet {

    @Id
    @Column(name = "tweet_id", nullable = false)
    private Long tweetId;

    @Id
    @Column(name = "retweeter", nullable = false)
    private String creator;

    @ManyToOne(optional = false)
    @JoinColumn(name = "tweet_id", nullable = false, insertable = false, updatable = false)
    private Tweet tweet;

    @ManyToOne(optional = false)
    @JoinColumn(name = "retweeter", nullable = false, insertable = false, updatable = false)
    private User retweeter;

    public Long getTweetId() {
        return tweetId;
    }

    public void setTweetId(Long tweetId) {
        this.tweetId = tweetId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Tweet getTweet() {
        return tweet;
    }

    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }

    public User getRetweeter() {
        return retweeter;
    }

    public void setRetweeter(User retweeter) {
        this.retweeter = retweeter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Retweet retweet = (Retweet) o;
        return Objects.equal(tweet, retweet.tweet) &&
                Objects.equal(retweeter, retweet.retweeter);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(tweet, retweeter);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tweet", tweet)
                .add("retweeter", retweeter)
                .add("time", time)
                .add("timeView", timeView)
                .toString();
    }
}
