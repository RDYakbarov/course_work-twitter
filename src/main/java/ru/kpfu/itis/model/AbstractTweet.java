package ru.kpfu.itis.model;

import ru.kpfu.itis.util.DateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by RDYakbarov on 01.05.15.
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractTweet implements Comparable<AbstractTweet>, Serializable {

    @Column(nullable = false)
    protected Long time;

    protected transient String timeView;

    public String getTimeView() {
        return timeView;
    }

    public void setTimeView(String timeView) {
        this.timeView = timeView;
    }

    @PostPersist
    @PostLoad
    void postLoad() {
        Long currentTime = new Date().getTime();
        long day = 24 * 60 * 60 * 1000;
        long dateDiff = currentTime - time;
        if (dateDiff > 365 * day) timeView = dateDiff / (365 * day) + " лет назад";
        else if (dateDiff > 7 * day) timeView = dateDiff / day + " дней назад";
        else timeView = DateUtil.dateFormat.format(new Date(time));
    }

    @PrePersist
    void prePersist() {
        time = new Date().getTime();
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public int compareTo(AbstractTweet abstractTweet) {
        return -time.compareTo(abstractTweet.time);
    }
}
