package ru.kpfu.itis.model.enums;

public enum ImagesFormat {
    GIF("gif"),
    JPG("jpg"),
    JPEG("jpeg"),
    BMP("bmp"),
    PNG("PNG");

    private String value;

    ImagesFormat(String s) {
        this.value=s;
    }

    public static ImagesFormat[] getAll(){
        return ImagesFormat.class.getEnumConstants();
    }

    public String toString() {
        return value;
    }

    public String getValue() {
        return this.value;
    }

    public String getFriendType() {
        return this.name();
    }

    public static boolean checkExistImageFormat(String type){
        if(type != null){
            for (ImagesFormat imagesFormat: ImagesFormat.values()){
                if(type.equalsIgnoreCase(imagesFormat.getValue()))
                    return true;
            }
        }
        return false;
    }
}
