package ru.kpfu.itis.util;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by RDYakbarov on 14.04.2015.
 */
class CustomFileNameFilter implements FilenameFilter {
    String fileFormat;
    String login;

    CustomFileNameFilter(String login, String fileFormat) {
        this.fileFormat = fileFormat;
        this.login = login;

    }
    @Override
    public boolean accept(File dir, String name) {
        return name.startsWith(login) && !name.endsWith(fileFormat);
    }
}