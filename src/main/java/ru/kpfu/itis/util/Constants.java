package ru.kpfu.itis.util;

/**
 * Created by RDYakbarov on 08.04.2015.
 */
public interface Constants {
    String UPLOAD_AVATAR_PREFIX = "/avatar/";
    String DEF_PIC = "http://s6.pikabu.ru/post_img/2015/02/23/9/1424707026_1045505907.jpg";
    int PAGE_SIZE = 20;
}
