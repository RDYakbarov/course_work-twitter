package ru.kpfu.itis.util;

import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.model.enums.ImagesFormat;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;

/**
 * Created by RDYakbarov on 08.04.2015.
 */
public class UploadAvatarUtil {

    public static String uploadAvatar(MultipartFile file, String login, String path) {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                File directory = new File(path);
                directory.mkdirs();
                int i = file.getOriginalFilename().lastIndexOf(".") + 1;
                String fileFormat = file.getOriginalFilename().substring(i);
                if (ImagesFormat.checkExistImageFormat(fileFormat)) {
                    String fileName = login + "." + fileFormat;
                    if (path.charAt(path.length() - 1) != File.separatorChar) {
                        path += File.separator;
                    }
                    File uploadFile = new File(path + fileName);
                    BufferedOutputStream stream =
                            new BufferedOutputStream(new FileOutputStream(uploadFile));
                    stream.write(bytes);
                    stream.close();
                    FilenameFilter filter = new CustomFileNameFilter(login, fileFormat);

                    File[] matchingFiles = directory.listFiles(filter);
                    for (int k = 0; k < matchingFiles.length; k++) {
                        matchingFiles[k].delete();
                    }
                    return Constants.UPLOAD_AVATAR_PREFIX + fileName;
                }
                return "incorrect avatar format";
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Файл не выбран");
        }
        return null;
    }
}
