package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.dto.UserDTO;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.validator.UserRegistrationValidator;

import javax.validation.Valid;

/**
 * Created by RDYakbarov on 31.03.2015.
 */
@Controller
public class RegistrationController {

    @Autowired
    UserService userService;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(new UserRegistrationValidator());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@Valid @ModelAttribute("userDTO") UserDTO userDTO, BindingResult result) {
        if (userService.userExists(userDTO.getLogin())) {
            result.rejectValue("login", "login.exist");
        }
        if (result.hasErrors()) {
            return "loginPage";
        }
        return "redirect:/" + userService.addUser(userDTO).getLogin();
    }
}
