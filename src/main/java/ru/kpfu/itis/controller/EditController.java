package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.dto.AvatarDTO;
import ru.kpfu.itis.model.User;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;
import ru.kpfu.itis.util.Constants;

import static ru.kpfu.itis.util.AuthenticationUtil.getAuthUser;
import static ru.kpfu.itis.util.AuthenticationUtil.updatePrincipalData;

/**
 * Created by RDYakbarov on 08.04.2015.
 */
@Controller
public class EditController {
    @Autowired
    UserService userService;

    @Autowired
    TweetService tweetService;

    @RequestMapping(value = "/editProfile", method = RequestMethod.GET)
    public String renderSocialRole(ModelMap map) {
        User user = getAuthUser();
        map.addAttribute("user", user);
        map.addAttribute("defPic", Constants.DEF_PIC);
        return "/editProfile";
    }

    @RequestMapping(value = "/ajax/photo/change", method = RequestMethod.POST)
    @ResponseBody
    public String uploadImage(@ModelAttribute("form") AvatarDTO form) {
        User user = userService.findByLogin(form.getLogin());
        String login = user.getLogin();
        String avatarUrl = userService.uploadAvatar(form.getFile(), login);
        if (avatarUrl != null && !avatarUrl.equals("incorrect avatar format")) {
            user.setAvatarURL(avatarUrl);
            userService.save(user);
            updatePrincipalData(user);
        }
        return user.getAvatarURL();
    }
}
