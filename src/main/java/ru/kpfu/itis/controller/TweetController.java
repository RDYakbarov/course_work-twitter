package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.dto.TweetDTO;
import ru.kpfu.itis.service.TweetService;
import ru.kpfu.itis.service.UserService;

import javax.validation.Valid;
import java.util.Collections;

/**
 * Created by RDYakbarov on 08.04.2015.
 */
@Controller
public class TweetController {

    @Autowired
    TweetService tweetService;

    @Autowired
    UserService userService;

    @ModelAttribute
    public TweetDTO tweetDTO() {
        return new TweetDTO();
    }

    @RequestMapping(value = "/ajax/tweet/add", method = RequestMethod.POST)
    public String createTweet(
            @Valid @ModelAttribute TweetDTO tweetDTO,
            ModelMap map,
            BindingResult bindingResult) throws BindException {
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        map.addAttribute("tweets", Collections.singletonList(tweetService.save(tweetDTO)));
        return "postsInclude";
    }

    @RequestMapping(value = "/ajax/answer/add", method = RequestMethod.POST)
    public String addAnswer(@Valid @ModelAttribute TweetDTO tweetDTO,
                            ModelMap map,
                            BindingResult bindingResult) throws BindException {
        // аннотации проверяют TweetDTO и выдают юзеру ошибку 400
        // The request sent by the client was syntactically incorrect.
        if (bindingResult.hasErrors()) {
            throw new BindException(bindingResult);
        }
        map.addAttribute("answer", tweetService.save(tweetDTO));
        return "tweetPage/commentInclude";
    }

}
